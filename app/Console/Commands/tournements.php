<?php

namespace App\Console\Commands;

use App\Models\Team;
use App\Models\Tournament;
use Faker\Factory;
use Illuminate\Console\Command;

class tournements extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tournements:create {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $faker = new Factory();
        $name = $this->argument('name') ?:  $faker->name();

        $tournament = Tournament::factory()->make([
            'name'=>$name
        ]);
        $tournament->save();
    }
}
