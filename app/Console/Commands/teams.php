<?php

namespace App\Console\Commands;

use Database\Factories\TeamFactory;
use Illuminate\Console\Command;
use Faker\Factory;
use App\Models\Team;
class teams extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'teams:create {name?} {power?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $faker = new Factory();
        $name = $this->argument('name') ?:  $faker->name();
        $power = $this->argument('power') ?: floor(random() * 100 + 1);

        $team = Team::factory()->make([
            'name' => $name,
            'power' => $power
        ]);
        $team->save();
    }
}
