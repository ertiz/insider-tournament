<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Team
 *
 * @property int $id
 * @property string $name
 * @property int $power
 *
 * @package App\Models
 */

class Team extends Model
{
    use HasFactory;
    const HOME = 'home';
    const AWAY = 'away';
	protected $table = 'teams';
	public $score;
	public $timestamps = false;

    protected $primaryKey = 'id';

	protected $casts = [
		'power' => 'int'
	];

	protected $fillable = [
		'name',
		'power'
	];

    public function allMatchesQueryByTournament($id)
    {
        $this->score = new Score();
        // This needs to be wrapped in a nested query or else your orWhere will not be contained in parentheses.
        return Matches::where(function($q) use ($id) {
            $q->where(function($q1){
                $q1->where('home_team', $this->id)->orWhere('away_team', $this->id);
            })->where('tournament_id',$id);

        });
    }

    public function getScore($tournamentId)
    {
        if(!is_null($this->score))
        {
            return $this->score;
        }
        foreach ($this->allMatchesQueryByTournament($tournamentId)->get() as $item)
        {
            $this->score->totalMatch++;
            $firstTeam = $item->home_team == $this->id ? self::HOME : self::AWAY;
            $secondTeam = $item->home_team == $this->id ? self::AWAY : self::HOME;
            if($item->home_goal ==  $item->away_goal)
            {
                $this->score->point += 1;
                $this->score->quits++;
            }else{
                if($item->{$firstTeam.'_goal'} >  $item->{$secondTeam.'_goal'})
                {
                    $this->score->point +=3;
                    $this->score->won++;
                }else{
                    $this->score->lose++;
                }
            }
            $this->score->goal += $item->{$firstTeam.'_goal'};
            $this->score->fail += $item->{$secondTeam.'_goal'};

        }
        return $this->score;
    }
    public function score()
    {
        return $this->score;
    }

}
