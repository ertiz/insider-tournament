<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tournament
 *
 * @property int $id
 * @property int $year
 *
 * @package App\Models
 */
class Tournament extends Model
{
    use HasFactory;
	protected $table = 'tournament';
	public $timestamps = false;

	protected $casts = [
	];

	protected $fillable = [
		'name'
	];
}
