<?php


namespace App\Models;


class Score
{
    public $point=0;
    public $goal=0;
    public $fail=0;
    public $quits=0;
    public $won=0;
    public $lose=0;
    public $totalMatch=0;
}
