<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Match
 *
 * @property int $id
 * @property int $home_team
 * @property int $away_team
 * @property int $tournement_id
 * @property int $week
 * @property int $home_goal
 * @property int $away_goal
 *
 * @package App\Models
 */
class Matches extends Model
{
	protected $table = 'match';
	public $timestamps = false;

    protected $primaryKey = 'id';

	protected $casts = [
		'home_team' => 'int',
		'away_team' => 'int',
		'tournament_id' => 'int',
		'week' => 'int',
		'home_goal' => 'int',
		'away_goal' => 'int'
	];

	protected $fillable = [
		'home_team',
		'away_team',
		'tournement_id',
		'week',
		'home_goal',
		'away_goal'
	];

}
