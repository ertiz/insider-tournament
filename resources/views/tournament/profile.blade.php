@extends('layout')

@section('title', $tournament->name)



@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Takım Adı</th>
            <th scope="col">Maç Sayısı</th>
            <th scope="col">Kazandı</th>
            <th scope="col">Berabere</th>
            <th scope="col">Kaybetti</th>
            <th scope="col">Gol Attı</th>
            <th scope="col">Gol Yedi</th>
            <th scope="col">Gol Farkı</th>
            <th scope="col">Puan</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($teams as $team)
            <tr>
                <th scope="row">{{ $team->id }}</th>
                <td>{{ $team->name }}</td>
                <td>{{ $team->getScore($tournament_id)->totalMatch }}</td>
                <td>{{ $team->getScore($tournament_id)->won }}</td>
                <td>{{ $team->getScore($tournament_id)->quits }}</td>
                <td>{{ $team->getScore($tournament_id)->lose }}</td>
                <td>{{ $team->getScore($tournament_id)->goal }}</td>
                <td>{{ $team->getScore($tournament_id)->fail }}</td>
                <td>{{ $team->getScore($tournament_id)->goal - $team->score()->fail }}</td>
                <td>{{ $team->getScore($tournament_id)->point }}</td>

            </tr>
        @endforeach

        </tbody>
    </table>
@stop
