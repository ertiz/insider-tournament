@extends('layout')

@section('title', 'Turnuvalar')



@section('content')
<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Adı</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    @foreach ($tournaments as $tournament)
        <tr>
            <th scope="row">{{ $tournament->id }}</th>
            <td>{{ $tournament->name }}</td>
            <td><a href="{{ action([\App\Http\Controllers\TournamentController::class, 'show'], ['id' => $tournament->id]) }}">
                    <span class="menu-text"> Lig Tablosu </span>
                </a>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>
@stop
